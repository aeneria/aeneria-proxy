<?php

declare(strict_types=1);

namespace App\Tests;

use App\Entity\PendingAction;
use App\Repository\PendingActionRepository;
use App\Services\JwtService;
use App\Services\PendingActionService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\HttpKernel\KernelInterface;

abstract class AppTestCase extends KernelTestCase
{
    private $_kernel;
    private $_entityManager;

    /**
     * Get kernel
     */
    final protected function getKernel(): KernelInterface
    {
        return $this->_kernel ?? (
            $this->_kernel = self::bootKernel()
        );
    }

    final protected static function getResourceDir(): string
    {
        return __DIR__ . '/Resources';
    }

    final protected function getEntityManager(): EntityManagerInterface
    {
        return $this->_entityManager ?? (
            $this->_entityManager = $this->getContainer()->get('doctrine')->getManager()
        );
    }

    final protected function getParameter(string $parameter): string
    {
        return $this->getContainer()->getParameter($parameter);
    }

    final protected function getPendingActionRepository(): PendingActionRepository
    {
        return $this->getEntityManager()->getRepository(PendingAction::class);
    }

    final protected function getPendingActionService(): PendingActionService
    {
        return new PendingActionService(
            $this->getEntityManager(),
            $this->getPendingActionRepository(),
        );
    }

    final protected function getJwtService(): JwtService
    {
        return new JwtService($this->getParameter('kernel.project_dir'));
    }

    /**
     * Create PendingAction from array
     */
    final protected function createPendingAction(array $data = []): PendingAction
    {
        return (new PendingAction())
            ->setId($data['id'] ?? \rand())
            ->setToken($data['token'] ?? 'token' . \rand())
            ->setAction($data['action'] ?? 'action')
            ->setExpirationDate($data['expirationDate'] ?? new \DateTimeImmutable())
            ->setParam($data['param'] ?? ['testParam' => 'testValue'])
        ;
    }

    final protected function createPersistedPendingAction(array $data = []): PendingAction
    {
        $pendingAction = $this->createPendingAction($data);
        $this->getEntityManager()->persist($pendingAction);

        return $pendingAction;
    }
}
