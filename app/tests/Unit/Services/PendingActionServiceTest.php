<?php

declare(strict_types=1);

namespace App\Tests\Unit\Services;

use App\Entity\PendingAction;
use App\Services\PendingActionService;
use App\Tests\AppTestCase;

final class PendingActionServiceTest extends AppTestCase
{
    public function testCreateDataConnectCallbackAction()
    {
        $actionService = new PendingActionService(
            $this->getEntityManager(),
            $this->getPendingActionRepository()
        );

        $action = $actionService->createAction(PendingAction::ACTION_DATA_CONNECT_CALLBACK, [
            'callback' => $callback = 'http://toto.com',
            'state' => $state = 'state' . \rand()
        ]);

        self::assertTrue($action->existParam('callback'));
        self::assertSame($action->getSingleParam('callback'), $callback);
        self::assertSame($action->getSingleParam('state'), $state);
        self::assertSame($action->getAction(), PendingAction::ACTION_DATA_CONNECT_CALLBACK);
        self::assertEquals(PendingAction::TOKEN_LENGTH, \strlen($action->getToken()));
        self::assertLessThan(new \DateTimeImmutable('now + 1 day'), $action->getExpirationDate());
    }

    public function testFindDataConnectCallbackAction()
    {
        $actionService = new PendingActionService(
            $this->getEntityManager(),
            $this->getPendingActionRepository()
        );

        $action = $actionService->createAction(PendingAction::ACTION_DATA_CONNECT_CALLBACK, [
            'callback' => $callback = 'http://toto.com',
            'state' => $state = 'state' . \rand()
        ]);

        $actionFromRepo = $actionService->findAction(PendingAction::ACTION_DATA_CONNECT_CALLBACK, $action->getToken());

        self::assertSame($action, $actionFromRepo);
    }

    public function testFindDataConnectCallbackActionUnknownToken()
    {
        $actionService = new PendingActionService(
            $this->getEntityManager(),
            $this->getPendingActionRepository()
        );

        $this->expectExceptionMessage("Impossible de trouver la demande correspondante");
        $actionService->findAction(PendingAction::ACTION_DATA_CONNECT_CALLBACK, 'totoot' . \rand());
    }
}
