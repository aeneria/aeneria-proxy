<?php

declare(strict_types=1);

namespace App\Services;

use App\Entity\GrdfAdictRecord;
use App\Repository\GrdfAdictRecordRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityNotFoundException;

/**
 * Data exporter service.
 */
class GrdfAdictRecordService
{
    public function __construct(
        private EntityManagerInterface $entityManager,
        private GrdfAdictRecordRepository $recordRepository,
        private JwtService $jwtService
    ) {}

    public function createRecord(string $pce, string $clientPublicKey): GrdfAdictRecord
    {
        $hash = \hash('md5', $pce);

        // On ne peut avoir qu'un consentement par pce
        if ($existings = $this->recordRepository->findByHashedPce($hash)) {
            foreach($existings as $existing) {
                $this->entityManager->remove($existing);
            }

            $this->entityManager->flush();
        }

        $record = (new GrdfAdictRecord())
            ->setCreatedAt(new \DateTimeImmutable())
            ->setClientPublicKey($clientPublicKey)
            ->setHashedPce($hash)
        ;

        $this->entityManager->persist($record);
        $this->entityManager->flush();

        $record
            ->setPce($pce)
            ->setEncodedPce($this->jwtService->encode($pce))
        ;

        return $record;
    }

    public function findRecord(string $encodePce): GrdfAdictRecord
    {
        $pce = (string)$this->jwtService->decode($encodePce);
        $hash = \hash('md5', $pce);

        if (!$record = $this->recordRepository->findOneByHashedPce($hash)) {
            throw new EntityNotFoundException('');
        }
        \assert($record instanceof GrdfAdictRecord);

        $record
            ->setPce($pce)
            ->setEncodedPce($encodePce)
        ;

        return $record;
    }
}
