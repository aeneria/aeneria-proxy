<?php

declare(strict_types=1);

namespace App\Services;

use App\Entity\EnedisDataConnectRecord;
use App\Repository\EnedisDataConnectRecordRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityNotFoundException;

/**
 * Data exporter service.
 */
class EnedisDataConnectRecordService
{
    public function __construct(
        private EntityManagerInterface $entityManager,
        private EnedisDataConnectRecordRepository $recordRepository,
        private JwtService $jwtService
    ) {}

    public function createRecord(string $pdl, string $clientPublicKey): EnedisDataConnectRecord
    {
        $hash = \hash('md5', $pdl);

        // On ne peut avoir qu'un consentement par pdl
        if ($existings = $this->recordRepository->findByHashedPdl($hash)) {
            foreach($existings as $existing) {
                $this->entityManager->remove($existing);
            }

            $this->entityManager->flush();
        }

        $record = (new EnedisDataConnectRecord())
            ->setCreatedAt(new \DateTimeImmutable())
            ->setClientPublicKey($clientPublicKey)
            ->setHashedPdl($hash)
        ;

        $this->entityManager->persist($record);
        $this->entityManager->flush();

        $record
            ->setPdl($pdl)
            ->setEncodedPdl($this->jwtService->encode($pdl))
        ;

        return $record;
    }

    public function findRecord(string $encodePdl): EnedisDataConnectRecord
    {
        $pdl = (string)$this->jwtService->decode($encodePdl);
        $hash = \hash('md5', $pdl);

        if (!$record = $this->recordRepository->findOneByHashedPdl($hash)) {
            throw new EntityNotFoundException('');
        }
        \assert($record instanceof EnedisDataConnectRecord);

        $record
            ->setPdl($pdl)
            ->setEncodedPdl($encodePdl)
        ;

        return $record;
    }
}
