<?php

declare(strict_types=1);

namespace App\Services;

use App\Entity\PendingAction;
use App\Entity\RequestLog;
use App\Repository\PendingActionRepository;
use App\Repository\RequestLogRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityNotFoundException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

/**
 * antiflood service.
 */
class AntiFloodService
{
    /** @var EntityManagerInterface */
    private $entityManager;

    /** @var RequestLogRepository */
    private $logRepository;

    public function __construct(EntityManagerInterface $entityManager, RequestLogRepository $logRepository)
    {
        $this->entityManager = $entityManager;
        $this->logRepository = $logRepository;
    }

    public function preventFlood(Request $request, ?string $action = null): void
    {
        if (!$ip = $request->getClientIp()) {
            throw new AccessDeniedHttpException('No ip provided.');
        }

        $action = $action ?? $request->getPathInfo();

        if ($this->logRepository->isFlooder($ip, $action)) {
            throw new AccessDeniedHttpException('Too many request.');
        }

        $log = (new RequestLog())
            ->setIp($ip)
            ->setAction($action)
            ->setDate(new \DateTimeImmutable('now'))
        ;

        $this->entityManager->persist($log);
        $this->entityManager->flush();
    }
}
