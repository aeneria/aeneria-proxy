<?php

declare(strict_types=1);

namespace App\Services;

use App\Entity\PendingAction;
use App\Repository\PendingActionRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityNotFoundException;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

/**
 * Data exporter service.
 */
class PendingActionService
{
    /** @var EntityManagerInterface */
    private $entityManager;

    /** @var PendingActionRepository */
    private $actionRepository;

    public function __construct(EntityManagerInterface $entityManager, PendingActionRepository $actionRepository)
    {
        $this->entityManager = $entityManager;
        $this->actionRepository = $actionRepository;
    }

    public function createAction(string $actionType, array $param): PendingAction
    {
        $token = \bin2hex(\openssl_random_pseudo_bytes(PendingAction::TOKEN_LENGTH / 2));

        $action = (new PendingAction())
            ->setToken($token)
            ->setAction($actionType)
            ->setExpirationDate(new \DateTimeImmutable('now + 1 day'))
            ->setParam($param)
        ;

        $this->entityManager->persist($action);
        $this->entityManager->flush();

        return $action;
    }

    public function findAction(string $actionType, string $token): PendingAction
    {
        if (!$action = $this->actionRepository->findOneByToken($token)) {
            throw new EntityNotFoundException('Impossible de trouver la demande correspondante');
        }
        \assert($action instanceof PendingAction);

        if ($action->getExpirationDate() < new \DateTimeImmutable()) {
            throw new AccessDeniedHttpException('Le token a expiré.');
        }

        if($action->getAction() !== $actionType) {
            throw new AccessDeniedHttpException('Le token ne correspond pas à l\'action demandée.');
        }

        return $action;
    }

    public function delete(PendingAction $action)
    {
        $this->entityManager->remove($action);
        $this->entityManager->flush();
    }
}
