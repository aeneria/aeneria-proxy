<?php

declare(strict_types=1);

namespace App\Controller;

use App\Entity\PendingAction;
use Aeneria\GrdfAdictApi\Client\GrdfAdictClientInterface;
use Aeneria\GrdfAdictApi\Model\Token;
use App\Services\AntiFloodService;
use App\Services\GrdfAdictRecordService;
use App\Services\JwtService;
use App\Services\PendingActionService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Exception\BadRequestException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Utilisation proxifiée d'une partie de l'api Grdf Adict
 *
 * Pour sécuriser l'utilisation de l'api - c'est à dire,
 * faire en sorte que n'importe quel utilisateur ne puisse par récupérer
 * les données du compteur gazpar d'un autre - un worflow particulier a
 * été mis en place :
 *
 * - Lorsque qu'un client fait une demande d'authorisation d'accès à un compteur (ie `::authorize()`)
 *   il fournit une clée RSA publique.
 * - On prend en note cette clée, elle sera ensuite utilisée pour chiffrer toutes les données
 *   issues de cette demande de consentement.
 * - Au retour de Grdf adict (ie `::callback()`), on récupère le PCE correspond au compteur pour
 *   lequel on vient d'avoir l'authorisation et on garde en bdd un `enregistrement` comportant :
 *   un hash de ce PCE et la clé RSA publique associée (celle envoyée à l'étape 1).
 *   On renvoie ensuite au client une version chiffrée (avec sa clé) du PCE et une version encodée
 *   (avec notre clé) de ce même PCE.
 *
 * Par la suite, pour utiliser l'api, le client fournira toujours le PCE encodé qu'on lui a fourni.
 *
 * De cette manière, à chaque appel d'API pour un PCE :
 * - on valide le PCE en le décodant, (si erreur au décodage => 404)
 * - on va ensuite chercher l'`enregistrement` en bdd correspond à ce PCE,
 *   (si pas d'`enregistrement` trouvé au décodage => 404)
 * - on fait l'appel à l'API, (si erreur d'appel => 500 avec le message)
 * - on renvoie les données chiffrées avec la clé RSA publique de l'`enregistrement`
 * - seul le client en possesion de la clé privée correspondante pourra lire la réponse
 *
 */
class GrdfAdictController extends AbstractController
{
    private ?Token $accessToken = null;

    public function __construct(
        private JwtService $jwtService,
        private PendingActionService $actionService,
        private AntiFloodService $antiFloodService,
        private GrdfAdictRecordService $recordService,
        private GrdfAdictClientInterface $grdfAdictClient
    ) {}

    public function authorize(Request $request): JsonResponse
    {
        if (!$data = $request->request->all()) {
            throw new BadRequestException("The body is empty.");
        }

        if (!(\array_key_exists('state', $data) && $state = $data['state'])) {
            throw new BadRequestException("A 'state' body parameter is required.");
        }
        if (!(\array_key_exists('key', $data) && $clientKey = $data['key'])) {
            throw new BadRequestException("A 'key' body parameter is required.");
        }
        if (!(\array_key_exists('callback', $data) && $callback = $data['callback'])) {
            throw new BadRequestException("A 'callback' body parameter is required.");
        }

        // We store callback and state parameter
        $action = $this->actionService->createAction(
            PendingAction::ACTION_GRDF_ADICT_CALLBACK,
            [
                'callback' => $callback,
                'state' => $state,
                'clientKey' => $clientKey,
            ]
        );

        $newState = $this->jwtService->encode($action->getToken());
        $consentPageUrl = $this
            ->grdfAdictClient
            ->getAuthentificationClient()
            ->getConsentPageUrl($newState, 'proxy.aeneria.com')
        ;

        return new JsonResponse($consentPageUrl, 200);
    }

    public function callback(Request $request): RedirectResponse
    {
        if (!$state = $request->query->get('state')) {
            throw new BadRequestException("A 'state' query parameter is required.");
        }

        try {
            $token = (string) $this->jwtService->decode($state);
        } catch (\Exception $e) {
            throw new BadRequestException("'state' parameter can't be decoded.");
        }

        $action = $this->actionService->findAction(
            PendingAction::ACTION_GRDF_ADICT_CALLBACK,
            $token
        );

        $clientKey = $action->getSingleParam('clientKey');
        $consentementDetail = $this
            ->grdfAdictClient
            ->getAuthentificationClient()
            ->requestConsentementDetail($request->query->get('code'))
        ;

        $record = $this->recordService->createRecord(
            $consentementDetail->pce,
            $clientKey
        );

        return $this->redirect(\sprintf(
            '%s?state=%s&encodedPce=%s',
            \urldecode($action->getSingleParam('callback')),
            $action->getSingleParam('state'),
            $record->getEncodedPce()
        ));
    }

    public function infoTechnique(string $encodedPce, Request $request): Response
    {
        $this->antiFloodService->preventFlood($request);

        try {
            $record = $this->recordService->findRecord($encodedPce);
        } catch (\Exception $e) {
            throw new NotFoundHttpException();
        }

        try {
            $info = $this->grdfAdictClient
                ->getContratClient()
                ->requestInfoTechnique(
                    $this->getAccessToken(),
                    $record->getPce()
                )
            ;

            // On évite de dépasser les quotas de GRDF,
            // on attends 1 seconde entre chaque requête
            \sleep(1);
        } catch (\Exception $e) {
            return new JsonResponse($e->getMessage(), 500);
        }

        $encryptedData = \sodium_crypto_box_seal(
            $info->rawData,
            $record->getDecodedClientPublicKey()
        );

        return new JsonResponse(
            ['data' => \urlencode($encryptedData)],
            200
        );
    }

    public function consoInformative(
        Request $request,
        string $encodedPce,
        string $dateDebut,
        string $dateFin
    ): Response {
        $this->antiFloodService->preventFlood($request);

        try {
            $dateDebut = new \DateTimeImmutable($dateDebut);
            $dateFin = new \DateTimeImmutable($dateFin);
        } catch (\Exception $e) {
            throw new BadRequestException('Les dates doivent être au format Y-m-d');
        }

        try {
            $record = $this->recordService->findRecord($encodedPce);
        } catch (\Exception $e) {
            throw new NotFoundHttpException();
        }

        try {
            $conso = $this->grdfAdictClient
                ->getConsommationClient()
                ->requestConsoInformative(
                    $this->getAccessToken(),
                    $record->getPce(),
                    $dateDebut,
                    $dateFin
                )
            ;

            // On évite de dépasser les quotas de GRDF,
            // on attends 1 seconde entre chaque requête
            \sleep(1);
        } catch (\Exception $e) {
            return new JsonResponse($e->getMessage(), 500);
        }

        $encryptedData = \sodium_crypto_box_seal(
            $conso->rawData,
            \urldecode($record->getDecodedClientPublicKey())
        );

        return new JsonResponse(
            ['data' => \urlencode($encryptedData)],
            200
        );
    }

    /**
     * Get a valid Access Token
     */
    private function getAccessToken(): string
    {
        if (!$this->accessToken || !$this->accessToken->isAccessTokenStillValid()) {
            $this->accessToken = $this
                ->grdfAdictClient
                ->getAuthentificationClient()
                ->requestAuthorizationToken()
            ;
        }

        return $this->accessToken->accessToken;
    }
}
