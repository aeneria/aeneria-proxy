<?php

declare(strict_types=1);

namespace App\Command;

use App\Services\JwtService;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class GenerateRsaKeyCommand extends Command
{
    /**
     * @var InputInterface
     */
    protected $defaultInput;

    /**
     * @var SymfonyStyle
     */
    protected $io;

    /** @var JwtService */
    private $jwtService;

    public function __construct(JwtService $jwtService)
    {
        $this->jwtService = $jwtService;
        parent::__construct();
    }

    protected function configure()
    {
        $this
            ->setName('aeneria-proxy:generate-rsa-key')
            ->setDescription('Generate RSA key.')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->defaultInput = $input;

        $this->io = new SymfonyStyle($input, $output);

        $this->jwtService->generateRsaKey();

        $this->io->success('RSA key has been successfully generated.');

        return 0;
    }
}
