<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20221222105130 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE grdf_adict_record ALTER hashedpce TYPE VARCHAR(255)');
        $this->addSql('ALTER TABLE grdf_adict_record ALTER client_public_key TYPE TEXT');
        $this->addSql('ALTER TABLE grdf_adict_record ALTER client_public_key DROP DEFAULT');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE grdf_adict_record ALTER hashedPce TYPE VARCHAR(50)');
        $this->addSql('ALTER TABLE grdf_adict_record ALTER client_public_key TYPE VARCHAR(255)');
        $this->addSql('ALTER TABLE grdf_adict_record ALTER client_public_key DROP DEFAULT');
    }
}
