<?php

declare(strict_types=1);

namespace App\Entity;

class EnedisDataConnectRecord
{
    /** @var int */
    private $id;

    /** @var null|string */
    private $hashedPdl;

    /** @var null|string */
    private $encodedPdl;

    /** @var null|string */
    private $pdl;

    /** @var string */
    private $clientPublicKey;

    /** @var \DateTimeInterface */
    private $createdAt;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(int $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getHashedPdl(): ?string
    {
        return $this->hashedPdl;
    }

    public function setHashedPdl(string $hashedPdl): self
    {
        $this->hashedPdl = $hashedPdl;

        return $this;
    }

    public function getEncodedPdl(): ?string
    {
        return $this->encodedPdl;
    }

    public function setEncodedPdl(string $encodedPdl): self
    {
        $this->encodedPdl = $encodedPdl;

        return $this;
    }

    public function getPdl(): ?string
    {
        return $this->pdl;
    }

    public function setPdl(string $pdl): self
    {
        $this->pdl = $pdl;

        return $this;
    }

    public function getClientPublicKey(): string
    {
        return $this->clientPublicKey;
    }

    public function getDecodedClientPublicKey(): string
    {
        return \urldecode($this->clientPublicKey);
    }

    public function setClientPublicKey(string $clientPublicKey): self
    {
        $this->clientPublicKey = $clientPublicKey;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }
}
