<?php

declare(strict_types=1);

namespace App\Entity;

class GrdfAdictRecord
{
    /** @var int */
    private $id;

    /** @var null|string */
    private $hashedPce;

    /** @var null|string */
    private $encodedPce;

    /** @var null|string */
    private $pce;

    /** @var string */
    private $clientPublicKey;

    /** @var \DateTimeInterface */
    private $createdAt;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(int $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getHashedPce(): ?string
    {
        return $this->hashedPce;
    }

    public function setHashedPce(string $hashedPce): self
    {
        $this->hashedPce = $hashedPce;

        return $this;
    }

    public function getEncodedPce(): ?string
    {
        return $this->encodedPce;
    }

    public function setEncodedPce(string $encodedPce): self
    {
        $this->encodedPce = $encodedPce;

        return $this;
    }

    public function getPce(): ?string
    {
        return $this->pce;
    }

    public function setPce(string $pce): self
    {
        $this->pce = $pce;

        return $this;
    }

    public function getClientPublicKey(): string
    {
        return $this->clientPublicKey;
    }

    public function getDecodedClientPublicKey(): string
    {
        return \urldecode($this->clientPublicKey);
    }

    public function setClientPublicKey(string $clientPublicKey): self
    {
        $this->clientPublicKey = $clientPublicKey;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }
}
